# Programación Orientada a Objetos Proyecto Primer Parcial 2020 – 1T

## Integrantes

- Abel Francisco Gómez Campuzano (_@abelfgomez_) (_afgomez@espol.edu.ec_)
- Ivan Ariel González Moreira (_@Arielin_gm_) (_ivargonz@espol.edu.ec_)
- Juan Antonio González Orbe (_@anntnzrb_) (_juangonz@espol.edu.ec_)

## Objetivos

Desarrollar en los estudiantes las habilidades de la programación orientada a
objetos básica incluyendo la aplicación de los conceptos de manejo de clases y
sus relaciones, paquetes, atributos y métodos de instancia y de clase, manejo
de listas, visibilidad de atributos y métodos, entrada por teclado y salida por
consola

## Credencienciales

| e-mail                   | contraseña    |
| ------------------------ | ------------- |
| `farmnearhome@gmail.com` | `proypoo2020` |
