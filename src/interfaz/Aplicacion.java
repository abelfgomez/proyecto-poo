package interfaz;

/*    ████████  ██████         ██  ████  */
/*   ██░░░░░░██░█░░░░        ██░  █░░░ █ */
/*  ██      ░░ ░█████      ██░   ░    ░█ */
/* ░██         ░░░░░ █   ██░        ███  */
/* ░██    █████     ░█  ░░ ██      ░░░ █ */
/* ░░██  ░░░░██ █   ░█    ░░ ██   █   ░█ */
/*  ░░████████ ░ ████       ░░ ██░ ████  */
/*   ░░░░░░░░   ░░░░          ░░  ░░░░   */

/* System */
import java.util.ArrayList;
import java.util.Scanner;

/* Local */
import pago.*;
import procesoventa.Carrito;
import procesoventa.Categorias;
import procesoventa.Pedido;
import procesoventa.Producto;
import registro.*;
import usuario.Cliente;
import usuario.Proveedor;

/**
 * Aplicacion principal
 *
 * @author Abel Gomez
 * @author Ariel Gonzalez
 * @author Juan Antonio Gonzalez
 */
final class Aplicacion {

    private static final Scanner scanf = new Scanner(System.in);
    private static byte opcion_menu, opcion_registro, opcion_iniciar_sesion;
    private static final byte MENU_TERMINAR = 3;

    private static final void menu() {
        /* Iniciarlizar datos */
        FarmNearHome fnh = new FarmNearHome();
        do {
            /**
             * ****************************************************************
             * Menu principal
             *****************************************************************
             */
            print_cabezera("Menu (Principal)");
            System.out.println("1. Registro");
            System.out.println("2. Iniciar Sesion");
            System.out.println("3. Salir");

            /* Solicitar opcion al usuario */
            try {
                print_pregunta("Ingrese opcion");
                opcion_menu = Byte.parseByte(scanf.nextLine());
            } catch(NumberFormatException nfe) {
                print_exception(nfe);
            }

            switch (opcion_menu) {
            /**
             * ****************************************************************
             * Opcion: 1. Registro
             *****************************************************************
             */
            case 1:
                print_cabezera("Menu (Registro)");

                /* Solicitar si desea registrarse como cliente o proveedor */
                System.out.println("Registrarse como proveedor o cliente?");
                System.out.println("1. Proveedor\n2. Cliente\n3. Regresar");

                try {
                    print_pregunta("Ingrese opcion");
                    opcion_registro = Byte.parseByte(scanf.nextLine());
                } catch(NumberFormatException nfe) {
                    print_exception(nfe);
                }

                switch (opcion_registro) {
                /* Registro Proveedor */
                case 1:
                    print_cabezera("Registro proveedor");
                    fnh.proveedores.add(RegistroProveedor.pedir_datos());
                    for (Proveedor pr : fnh.proveedores) {
                        System.out.println(pr);
                    }
                    break;
                /* Registro Proveedor */
                case 2:
                    print_cabezera("Registro cliente");
                    Cliente cl = RegistroCliente.pedir_datos();
                    System.out.println("Ingrese Metodo de pago");
                    System.out.println("1. Tarjeta\n2. Paypal");
                    print_pregunta("Ingrese opcion");
                    byte op_metodopago = Byte.parseByte(scanf.nextLine());
                    switch (op_metodopago) {
                    case 1:
                        cl.agregar_metodo_pago(PagoTarjeta.pedir_datos(cl));
                        break;
                    case 2:
                        cl.agregar_metodo_pago(PagoPaypal.pedir_datos());
                        break;
                    }
                    fnh.clientes.add(cl);
                    for (Cliente pr : fnh.clientes) {
                        System.out.println(pr);
                    }
                    break;
                /* Salir */
                case 3:
                    break;
                }
                break;
            /**
             * ****************************************************************
             * Opcion: 2. Iniciar Sesion
             *****************************************************************
             */
            case 2:
                print_cabezera("Menu (Inicio de Sesion)");

                /* Solicitar si desea iniciar sesion como cliente o proveedor */
                System.out.println("Iniciar sesion como proveedor o cliente?");
                System.out.println("1. Proveedor\n2. Cliente\n3. Regresar");

                try {
                    print_pregunta("Ingrese opcion");
                    opcion_iniciar_sesion = Byte.parseByte(scanf.nextLine());
                } catch(NumberFormatException nfe) {
                    print_exception(nfe);
                }

                switch (opcion_iniciar_sesion) {
                case 1:
                    for (Proveedor pr : fnh.proveedores) {
                        System.out.println(pr);
                    }
                    Proveedor proveedor = (Proveedor)
                                          FarmNearHome.iniciar_sesion(
                        "Proveedor");
                    if (proveedor != null) {
                        sub_menu(proveedor);
                        byte opcion_sub_menu = 0;
                        try {
                            print_pregunta("Ingrese opcion");
                            opcion_sub_menu = Byte.parseByte(scanf.nextLine());
                        } catch(NumberFormatException nfe) {
                            print_exception(nfe);
                        }
                        switch (opcion_sub_menu) {
                        case 1:
                            proveedor.registrar_productos(
                                proveedor.pedir_datos(proveedor));
                            break;
                        case 2:
                            proveedor.consultar_pedidos();
                            print_pregunta("Desea gestionar estado de pedido");
                            String op = scanf.nextLine();
                            while (op.toUpperCase().equals("SI")) {
                                System.out.println(
                                    "Ingrese el codigo del producto a gestionar");
                                proveedor.gestionar_estado_pedido(
                                    scanf.nextLine());
                                print_pregunta(
                                    "Desea seguir gestionando el estado de los pedidos");
                                op = scanf.nextLine();
                            }
                            break;
                        case 3:
                            System.out.println("Desea ingresar filtros");
                            System.out.println(
                                "Ingrese nombre del producto/Enter");
                            String filtro_nombre = scanf.nextLine();
                            System.out.println(
                                "Ingrese categoria del producto/Enter");
                            String filtro_cate = scanf.nextLine();
                            if (filtro_nombre.isBlank() &&
                                filtro_cate.isBlank()) {
                                proveedor.editar_productos();
                            } else if (!filtro_nombre.isBlank() &&
                                       filtro_cate.isBlank()) {
                                proveedor.editar_productos(filtro_nombre);
                            } else if (filtro_nombre.isBlank() &&
                                       !filtro_cate.isBlank()) {
                                proveedor.editar_productos(Categorias.valueOf(
                                                               filtro_cate.
                                                               toUpperCase()));
                            } else {
                                proveedor.editar_productos(filtro_nombre,
                                                           Categorias.valueOf(
                                                               filtro_cate.
                                                               toUpperCase()));
                            }
                            break;
                        case 4:
                            break;
                        }
                    }
                    break;
                case 2:
                    ArrayList < Pedido > pedidos_del_cliente;
                    Cliente cliente = (Cliente) FarmNearHome.iniciar_sesion(
                        "Cliente");
                    Carrito carrito = new Carrito(cliente);
                    if (cliente != null) {
                        sub_menu(cliente);
                        byte opcion_sub_menu = 0;
                        try {
                            print_pregunta("Ingrese opcion");
                            opcion_sub_menu = Byte.parseByte(scanf.nextLine());
                        } catch(NumberFormatException nfe) {
                            print_exception(nfe);
                        }

                        switch (opcion_sub_menu) {
                        case 1:
                            ArrayList < Proveedor >
                            prove_cercanos = fnh.productos_cercanos(cliente);
                            System.out.println("Desea ingresar filtros");
                            System.out.println(
                                "Ingrese nombre del producto/Enter");
                            String filtro_nombre = scanf.nextLine();
                            System.out.println(
                                "Ingrese categoria del producto/Enter");
                            String filtro_cate = scanf.nextLine();
                            for (Proveedor prove :prove_cercanos) {
                                if (filtro_nombre.isBlank() &&
                                    filtro_cate.isBlank()) {
                                    prove.consultar_productos();
                                } else if (!filtro_nombre.isBlank() &&
                                           filtro_cate.isBlank()) {
                                    prove.consultar_productos(filtro_nombre);
                                } else if (filtro_nombre.isBlank() &&
                                           !filtro_cate.isBlank()) {
                                    prove.consultar_productos(Categorias.valueOf(
                                                                  filtro_cate.
                                                                  toUpperCase()));
                                } else {
                                    prove.consultar_productos(filtro_nombre,
                                                              Categorias.valueOf(
                                                                  filtro_cate.
                                                                  toUpperCase()));
                                }

                            }
                            System.out.println("Codigo de producto a consultar");
                            String codigo_producto = scanf.nextLine();
                            Producto producto_selec = null;
                            for (Proveedor prove :prove_cercanos) {
                                for (Producto producto:
                                     prove.getLista_productos()) {
                                    if (producto.getCodigo().equals(
                                            codigo_producto)) {
                                        producto_selec = producto;
                                    }
                                }
                            }
                            System.out.println(producto_selec);
                            print_pregunta(
                                "Desea agregar el producto al carrito SI/NO");
                            String opcion1 = scanf.nextLine();
                            if (opcion1.toUpperCase().equals("SI")) {
                                print_pregunta("Ingrese la cantidad");
                                int opcion2 =
                                    Integer.parseInt(scanf.nextLine());
                                carrito.agregar_producto(producto_selec,
                                                         opcion2);
                            }
                            break;
                        case 2:
                            carrito.consultar_carrito();
                            System.out.println("1.Desea eliminar un carrito");
                            System.out.println("2.Desea efectuar la compra");
                            byte opcion3 = 0;
                            try {
                                print_pregunta("Ingrese opcion");
                                opcion3 = Byte.parseByte(scanf.nextLine());
                            } catch(NumberFormatException nfe) {
                                print_exception(nfe);
                            }
                            switch (opcion3) {
                            case 1:
                                String codigo_eliminar = scanf.nextLine();
                                carrito.eliminar_producto(codigo_eliminar);
                                break;
                            case 2:
                                pedidos_del_cliente =
                                    carrito.efectuar_compra();
                                cliente.setLista_pedido(pedidos_del_cliente);
                                System.out.println("La compra se realizo");
                                for (Pedido pd: pedidos_del_cliente) {
                                    for (Proveedor pro : fnh.proveedores) {
                                        if (pro.equals(pd.getProveedor())) {
                                            pro.ingresar_pedido(pd);
                                        }
                                    }
                                }
                                break;
                            }
                            break;
                        case 3:
                            for (Pedido p :
                                 cliente.getLista_pedido()) System.out.println(
                                    p);
                            break;
                        case 4:
                            break;
                        }
                    }
                    break;
                case 3:
                    break;
                }
                break;
            /**
             * ****************************************************************
             * Opcion: 3. Salir
             *****************************************************************
             */
            case MENU_TERMINAR:
                print_cabezera("\nGracias por utilizar nuestro servicio.\n");
                break;
            /**
             * ****************************************************************
             * Opcion: Invalida
             *****************************************************************
             */
            default:
                System.out.println("XXX Opcion invalida");
                break;
            }
        } while (opcion_menu != MENU_TERMINAR);
    }

    static final void print_cabezera(String msg) {
        System.out.printf("%n%s%n", "*".repeat(44));
        System.out.printf(" %s \n", msg);
        System.out.printf("%s%n", "*".repeat(44));
    }

    static final void print_pregunta(String msg) {
        System.out.printf(" --> %s: ", msg);
    }

    static final void print_exception(Exception e) {
        System.out.printf("%n%s%n", "X".repeat(79));
        System.out.printf("Error: tipo de dato ingresado invalido --> %s\n",
                          e.getMessage());
        System.out.printf("%s%n", "X".repeat(79));
    }

    static final void sub_menu(Proveedor pr) {
        print_cabezera("Bienvenido " + pr.getNombre());
        System.out.println("1. Registrar Producto");
        System.out.println("2. Consultar Informacion de los Pedidos");
        System.out.println("3. Consultar y Editar Informacion de los Productos");
        System.out.println("4. Atras");
    }

    static final void sub_menu(Cliente cl) {
        print_cabezera("Bienvenido " + cl.getNombre());
        System.out.println("1. Consultar Informacion de Productos");
        System.out.println("2. Consultar Informacion del Carrito");
        System.out.println("3. Consultar Pedidos Realizados");
        System.out.println("4. Atras");
    }

    /* Main */
    public static void main(String... args) {

        /* Mostrar menu */
        menu();
    }
}
