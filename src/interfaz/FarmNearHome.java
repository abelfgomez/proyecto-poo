package interfaz;

/* System */
import java.util.ArrayList;
import java.util.Scanner;

/* Local */
import static interfaz.Aplicacion.*;
import pago.*;
import procesoventa.Categorias;
import procesoventa.Producto;
import ubicacion.Coordenada;
import usuario.*;

final class FarmNearHome {
    /* Atributos */
    private static final Scanner scanf = new Scanner(System.in);
    static ArrayList < Proveedor > proveedores;
    static ArrayList < Cliente > clientes;
    /* Atributos de personas */
    private static String usuario, password;

    /* Constructor */
    public FarmNearHome() {
        proveedores = new ArrayList < > ();
        clientes = new ArrayList < > ();
        /* Cargar Datos */
        inicializarSistema();
    }

    private void inicializarSistema() {
        /**********************************************************************
        * Proveedores
        **********************************************************************/
        /* Proveedor 1 */
        Proveedor prov1 = new Proveedor(new Coordenada(-2.216346, -79.909717),
                                        "123",
                                        "juanantonio-go@hotmail.com",
                                        "joelol",
                                        "Joe",
                                        "093776787");
        proveedores.add(prov1);
        /* Proveedor 2 */
        Proveedor prov2 = new Proveedor(new Coordenada(-2.192458, -79.904868),
                                        "555",
                                        "ivanariel5@gmail.com",
                                        "arielin",
                                        "Ariel",
                                        "096649687");
        proveedores.add(prov2);
        /* Proveedor 3 */
        Proveedor prov3 = new Proveedor(new Coordenada(-1.828846, -79.248842),
                                        "123",
                                        "afgomez@espol.edu.ec",
                                        "pancho",
                                        "Francisco",
                                        "096128687");
        proveedores.add(prov3);
        /**********************************************************************
        * Clientes
        **********************************************************************/
        /* Cliente 1 */
        Cliente cte1 = new Cliente(new Coordenada(-2.212688, -79.903826),
                                   "123", "juangonz@espol.edu.ec", "juanlol",
                                   "Juan");
        cte1.agregar_metodo_pago(new PagoPaypal("Emiliano", "1235"));
        clientes.add(cte1);
        /* Cliente 2 */
        Cliente cte2 = new Cliente(new Coordenada(-2.067196, -79.892853),
                                   "123", "ivargonz@espol.edu.ec", "ivanval",
                                   "Ivan");
        cte2.agregar_metodo_pago(new PagoPaypal("Pedro", "1235"));
        clientes.add(cte2);
        /* Cliente 3 */
        Cliente cte3 = new Cliente(new Coordenada(-1.704118, -79.833848),
                                   "123", "abelgomezc2000@gmail.com", "ezzio",
                                   "Abel gomez");
        cte3.agregar_metodo_pago(new PagoTarjeta(Tipo_Tarjeta.VISA,
                                                 "79876543214335623",
                                                 "Alfredo", cte3));
        clientes.add(cte3);
        /**********************************************************************
        * Productos
        **********************************************************************/
        /* Producto 1 */
        prov1.registrar_productos(new Producto("001", "leche", 1.50, prov1,
                                               Categorias.LACTEOS));
        /* Producto 2 */
        prov2.registrar_productos(new Producto("002", "carne", 9.75, prov2,
                                               Categorias.CARNICOS));
        /* Producto 3 */
        prov3.registrar_productos(new Producto("003", "legumbres", 3.25, prov3,
                                               Categorias.VEGETALES));
    }
    public ArrayList < Proveedor > productos_cercanos(Cliente cliente) {
        ArrayList < Proveedor > list_provedor_cercanos = new ArrayList < > ();
        for (Proveedor prov : proveedores) {
            if (Coordenada.calcularDistancia(prov.getUbicacion(),
                                             cliente.getUbicacion()) <= 50) {
                list_provedor_cercanos.add(prov);
            }
        }
        return list_provedor_cercanos;
    }

    static final Persona iniciar_sesion(String tipo) {
        if (tipo.equals("Proveedor")) {
            /* Pedir datos de inicio de sesion provedor */
            print_cabezera("Inicio de sesion proveedor");
            print_pregunta("Ingrese su usuario");
            usuario = scanf.nextLine();
            print_pregunta("Ingrese su contrasena");
            password = scanf.nextLine();
            for (Proveedor pro : proveedores) {
                if (pro.getId().equals(usuario) &&
                    pro.getClave().equals(password)) {
                    System.out.println("Sesion iniciada correctamente");
                    return pro;
                }
            }
        } else if (tipo.equals("Cliente")) {
            /* Pedir datos de inicio de sesion Cliente */
            print_cabezera("Inicio de sesion Cliente");
            print_pregunta("Ingrese su usuario");
            usuario = scanf.nextLine();
            print_pregunta("Ingrese su contrasena");
            password = scanf.nextLine();
            for (Cliente cliente : clientes) {
                if (cliente.getId().equals(usuario) &&
                    cliente.getClave().equals(password)) {
                    System.out.println("Sesion iniciada correctamente");
                    return cliente;
                }
            }
        }
        System.out.println("Usuario o contrasena Invalidas");
        return null;
    }
}
