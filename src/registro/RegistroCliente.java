package registro;

/* Local */
import ubicacion.Coordenada;
import usuario.Cliente;

public class RegistroCliente extends Registro {
    /* Metodos */
    /**
     *  Registra a un cliente a partir de distintos atributos
     *
     * @return {@link Cliente}
     */
    public static Cliente pedir_datos() {
        double latitud = 0;
        double longitud = 0;

        System.out.print("Ingrese su nombre: ");
        String nombre = scanf.nextLine();
        System.out.print("Ingrese su e-mail: ");
        String email = scanf.nextLine();
        System.out.print("Ingrese su clave: ");
        String clave = scanf.nextLine();
        System.out.print("Ingrese su identificacion (ID): ");
        String id = scanf.nextLine();

        try {
            System.out.print("Ingrese su latitud: ");
            latitud = Double.parseDouble(scanf.nextLine());
        }
        catch(NumberFormatException nfe) {
            System.out.printf(
                "Error: tipo de dato ingresado invalido --> %s\n",
                nfe);
        }
        try {
            System.out.print("Ingrese su longitud: ");
            longitud = Double.parseDouble(scanf.nextLine());
        }
        catch(NumberFormatException nfe) {
            System.out.printf(
                "Error: tipo de dato ingresado invalido --> %s\n",
                nfe);
        }
        Coordenada ubicacion = new Coordenada(latitud, longitud);
        Cliente cte = new Cliente(ubicacion, clave, email, id, nombre);

        return cte;
    }
}
