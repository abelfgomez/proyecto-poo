package ubicacion;

/* System */
import static java.lang.Math.*;

public class Coordenada {
    /* Atributos */
    private double latitud, longitud;
    private static final double RADIO_TIERRA = 6378.137;

    /* Constructor */
    public Coordenada(double lat, double longi) {
        this.latitud = lat;
        this.longitud = longi;
    }

    /* Metodos */
    /**
     * Metodo estatico que calcula la distancia Harvesine
     *
     * @param c1 ubicacion 1
     * @param c2 ubicacion 2
     * @return disntancia entre dos coordenadas
     */
    public static double calcularDistancia(Coordenada c1, Coordenada c2) {
        double diff_lat = c2.latitud - c1.latitud;
        double diff_longi = c2.longitud - c1.longitud;
        double a = pow(sin(toRadians(diff_lat) / 2), 2) +
                   cos(toRadians(c1.latitud)) * cos(toRadians(c2.latitud)) *
                   pow(sin(toRadians(diff_longi) / 2), 2);
        return RADIO_TIERRA * (2 * atan2(sqrt(a), sqrt(1 - a)));
    }

    @Override
    public String toString() {
        return "latitud: " + latitud + ", longitud: " + longitud;
    }

    /* Getters & Setters */
    public static double getRADIO_TIERRA() { return RADIO_TIERRA; }
    public double getlat() { return latitud; }
    public double getlongi() { return longitud; }
    public void   setlat(double lat) { this.latitud = lat; }
    public void   setlongi(double longi) { this.longitud = longi; }

}
