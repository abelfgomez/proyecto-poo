package procesoventa;

/* System */
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;

/* Local */
import usuario.Cliente;
import usuario.Proveedor;
import pago.*;

public class Carrito implements InterfazGeneral {
    /* Atributos */
    private ArrayList < DetalleProducto > carrito_compra;
    private Cliente cliente;
    private double total_pagar;

    /* Constructor */
    public Carrito( Cliente cliente) {
        carrito_compra = new ArrayList < > ();
        this.cliente = cliente;
    }
    public void consultar_carrito() {
        for (DetalleProducto dp : carrito_compra) {
            System.out.println(dp);
        }
    }

    public ArrayList < DetalleProducto > eliminar_producto(String codigo) {
        for (DetalleProducto dp : carrito_compra) {
            if (dp.getProducto().getCodigo().equals(codigo)) {
                carrito_compra.remove(dp);
            }
        }
        return carrito_compra;
    }

    public void agregar_producto(Producto produc, int cantidad ) {
        carrito_compra.add(new DetalleProducto(produc, cantidad));
    }

    /* Metodos */
    public double calcular_precio() {
        double precio_a_pagar = 0;
        for (DetalleProducto dp: carrito_compra) {
            precio_a_pagar += dp.getCantidad() * dp.getProducto().getPrecio();
        }
        return precio_a_pagar;
    }

    public String codigo_pedido() {
        return String.valueOf((int) (1000 + Math.random() * 9000));
    }

    public ArrayList < Pedido > efectuar_compra() {
        double precioApagar = calcular_precio();
        if (cliente.getForma_pago().proceso_pago(precioApagar)) {
            ArrayList < Proveedor > lista_provedor = new ArrayList < > ();
            ArrayList < Pedido > lista_pedido = new ArrayList < > ();
            for (DetalleProducto dp : carrito_compra) {
                if (!lista_provedor.contains(dp.getProducto().getProveedor()))
                {
                    Pedido pedido = new Pedido(LocalDateTime.now(),
                                               cliente.getForma_pago(),
                                               codigo_pedido(),
                                               Estado_Pedido.SOLICITADO,
                                               dp.getProducto().getProveedor(),
                                               cliente,
                                               dp);
                    lista_provedor.add(dp.getProducto().getProveedor());
                    lista_pedido.add(pedido);
                } else {
                    for (Pedido ped: lista_pedido) {
                        if (ped.getProveedor().equals(dp.getProducto().
                                                      getProveedor())) {
                            ped.agregar_producto(dp);
                        }
                    }
                }
            }
            String mensaje = "";
            for (Pedido p : lista_pedido) {
                mensaje += p.toString() + "\n";
            }
            InterfazGeneral.enviar_correo(cliente.getEmail(),
                                          "Informacion de la compra", mensaje);
            return lista_pedido;
        }
        return null;
    }
}
