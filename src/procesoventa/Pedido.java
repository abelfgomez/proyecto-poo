package procesoventa;

/* System */
import usuario.Cliente;
import java.time.LocalDateTime;
import java.util.ArrayList;

/* Local */
import usuario.Proveedor;
import pago.MetodoPago;

public class Pedido {
    /* Atributos */
    private LocalDateTime fecha;
    private LocalDateTime fecha_procesando;
    private LocalDateTime fecha_entregado;
    private MetodoPago metodo_pago;
    private String codigo_pedido;
    private Estado_Pedido estado_pedido;
    private Proveedor proveedor;
    private Cliente cliente;
    private ArrayList < DetalleProducto > dproductos;

    /* Constructor */
    public Pedido(LocalDateTime fecha, MetodoPago metodo_pago,
                  String codigo_pedido, Estado_Pedido estado_pedido,
                  Proveedor proveedor, Cliente cliente,
                  DetalleProducto producto) {
        this.fecha = fecha;
        this.metodo_pago = metodo_pago;
        this.codigo_pedido = codigo_pedido;
        this.estado_pedido = estado_pedido;
        this.proveedor = proveedor;
        this.cliente = cliente;
        /* Iniciar arreglo de productos */
        dproductos = new ArrayList < > ();
        this.dproductos.add(producto);
    }

    /* Metodos */
    /**
     * Agrega un {@link Producto} al arreglo de {@link DetalleProducto}
     *
     * @param producto
     */
    public void agregar_producto(DetalleProducto producto) {
        this.dproductos.add(producto);
    }

    /**
     *  Muestra distintos detalles acerca del {@link Pedido}
     *
     */
    public void mostrar_detalle_pedido() {
        System.out.println("Detalles del pedido");
        System.out.println("Fecha Solicitado: " + fecha);
        System.out.println("Fecha Procesando: " + fecha_procesando);
        System.out.println("Fecha Entregado: " + fecha_entregado);
        System.out.println("Codigo del pedido: " + codigo_pedido);
        System.out.println("Proveedor: " + proveedor);
        System.out.println("Cliente: " + cliente);
        System.out.println("Estado del pedido: " + estado_pedido);
        System.out.println("Metodo de pago: " + metodo_pago);
        System.out.println("Detalles de los productos");
        for (DetalleProducto dp : dproductos) {
            System.out.println(dp);
        }
    }

    @Override
    public String toString() {
        return "fecha solicitado: " + fecha +"fecha procesando: " + fecha_procesando +
                "fecha entregado: " + fecha_entregado +", metodo_pago: " + metodo_pago +
               ", codigo_pedido: " + codigo_pedido + ", estado_pedido: " +
               estado_pedido + ", proveedor: " + proveedor + ", cliente: " +
               cliente + ", dproductos: " + dproductos;
    }

    /* Getters & Setters */
    public LocalDateTime getFecha() { return fecha; }
    public MetodoPago getMetodo_pago() { return metodo_pago; }
    public String getCodigo_pedido() { return codigo_pedido; }
    public Estado_Pedido getEstado_pedido() { return estado_pedido; }
    public Proveedor getProveedor() { return proveedor; }
    public ArrayList < DetalleProducto > getProductos() {
        return dproductos;
    }
    public void setEstado_pedido(Estado_Pedido estado_pedido) {
        this.estado_pedido = estado_pedido;
    }

    public LocalDateTime getFecha_procesando() {
        return fecha_procesando;
    }

    public void setFecha_procesando(LocalDateTime fecha_procesando) {
        this.fecha_procesando = fecha_procesando;
    }

    public LocalDateTime getFecha_entregado() {
        return fecha_entregado;
    }

    public void setFecha_entregado(LocalDateTime fecha_entregado) {
        this.fecha_entregado = fecha_entregado;
    }
    
}
