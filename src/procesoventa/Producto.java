package procesoventa;

/* System */
import java.util.ArrayList;

/* Local */
import usuario.Proveedor;

public class Producto {

    /* Atributos */
    private String codigo;
    private String nombre;
    private double precio;
    private Proveedor proveedor;
    private Categorias categoria;

    /* Constructor */
    public Producto(String codigo, String nombre, double precio,
                    Proveedor proveedor, Categorias categoria) {
        this.codigo = codigo;
        this.nombre = nombre;
        this.precio = precio;
        this.proveedor = proveedor;
        this.categoria = categoria;
    }

    public Producto() {}

    /* Metodos */
    @Override
    public String toString() {
        return "Producto: " +"Proveedor="+proveedor+ ", codigo= " + codigo + 
                ", categoria="+categoria+", nombre= " + nombre +
               ", precio= " + precio;
    }

    /* Getters & Setters */
    public String getCodigo() { return codigo; }
    public void   setCodigo(String codigo) { this.codigo = codigo; }
    public String getNombre() { return nombre; }
    public void   setNombre(String nombre) { this.nombre = nombre; }
    public double getPrecio() { return precio; }
    public void   setPrecio(double precio) { this.precio = precio; }
    public Proveedor getProveedor() { return proveedor; }
    public Categorias getCategoria() { return categoria; }
    public void setCategoria(Categorias categoria) {
        this.categoria = categoria;
    }
}
