package procesoventa;

public class DetalleProducto {
    /* Atributos */
    private Producto producto;
    private int cantidad;

    /* Constructor */
    public DetalleProducto(Producto producto, int cantidad) {
        this.producto = producto;
        this.cantidad = cantidad;
    }

    public DetalleProducto() {}

    /* Metodos */
    public void mostrar_detalle_producto() {
        System.out.println(producto + "cantidad= " + cantidad);
    }

    /* Getters & Setters */
    public Producto getProducto() { return producto; }
    public void     setProducto(Producto prod) { this.producto = prod; }
    public int      getCantidad() { return cantidad; }
    public void     setCantidad(int cantidad) { this.cantidad = cantidad; }

    @Override
    public String toString() {
        return "producto" + producto + ", cantidad" + cantidad ;
    }   
}
