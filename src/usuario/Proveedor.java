package usuario;

/* System */
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Scanner;

/* Local */
import procesoventa.Pedido;
import procesoventa.Producto;
import ubicacion.Coordenada;
import procesoventa.Categorias;
import procesoventa.Estado_Pedido;

public class Proveedor extends Persona {
    private final Scanner scanf;
    private String num_contacto;
    private ArrayList < Producto > lista_productos;
    private ArrayList < Pedido > lista_pedidos;

    /* Constructor */
    public Proveedor(Coordenada ubicacion, String clave, String email,
                     String id, String nombre, String num_contacto) {
        super(ubicacion, clave, email, id, nombre);
        this.num_contacto = num_contacto;
        lista_productos = new ArrayList < > ();
        lista_pedidos = new ArrayList < > ();
        /* Iniciar Scanner */
        scanf = new Scanner(System.in);
    }

    /**
     * Registran {@link Producto} que seran vendidos por los proveedores
     *
     * @param producto a registrar
     */
    public void registrar_productos(Producto producto) {
        lista_productos.add(producto);
    }

    /**
     * Solicita datos para registrar un producto
     *
     * @param prov
     * @return
     */
    public Producto pedir_datos(Proveedor prov) {
        double precio = 0;

        System.out.print("Ingrese el codigo: ");
        String codigo = scanf.nextLine();
        System.out.print("Ingrese el nombre: ");
        String nombre = scanf.nextLine();
        try {
            System.out.print("Ingrese el precio: ");
            precio = Double.parseDouble(scanf.nextLine());
        }
        catch(NumberFormatException nfe) {
            System.out.printf(
                "Error: tipo de dato ingresado invalido --> %s\n",
                nfe);
        }
        System.out.print("Ingrese la categoria: ");
        String categoria = scanf.nextLine();

        Producto prod = new Producto(codigo, nombre, precio, prov,
                                     Categorias.valueOf(
                                         categoria.toUpperCase()));

        return prod;
    }

    /**
     * Registran {@link Producto} que seran vendidos por los proveedores
     */
    public void consultar_productos() {
        for (Producto prod : lista_productos) { System.out.println(prod); }
    }

    /**
     * Registran {@link Producto} que seran vendidos por los proveedores
     *
     * @param nombre
     */
    public void consultar_productos(String nombre) {
        for (Producto prod : lista_productos) {
            if (prod.getNombre().toUpperCase().contains(nombre.toUpperCase()))
            {
                System.out.println(prod);
            }
        }
    }

    /**
     *  Se consultan {@link Producto} vendidos por los proveedores
     *
     * @param categoria
     */
    public void consultar_productos(Categorias categoria) {
        for (Producto prod : lista_productos) {
            if (prod.getCategoria().equals(categoria)) {
                System.out.println(prod);
            }
        }
    }

    /**
     *  Se consultan {@link Producto} vendidos por los proveedores
     *
     * @param nombre
     * @param categoria
     */
    public void consultar_productos(String nombre, Categorias categoria) {
        for (Producto prod : lista_productos) {
            if (prod.getCategoria().equals(categoria) &&
                prod.getNombre().toUpperCase().contains(nombre.toUpperCase()))
            {
                System.out.println(prod);
            }
        }
    }

    /**
     * Editar informacion de {@link Producto} registrados
     *
     */
    public void editar_productos() {
        byte opcion_menu;

        consultar_productos();
        System.out.print("Ingrese codigo del producto a editar: ");
        String codigo = scanf.nextLine();
        for (Producto prod : lista_productos) {
            if (prod.getCodigo().equals(codigo)) {
                do {
                    System.out.println("Seleccione parametro a editar");
                    System.out.println("1.Nombre\n2.Precio\n3.Categoria");
                    opcion_menu = Byte.parseByte(scanf.nextLine());
                    switch (opcion_menu) {
                    case 1:
                        System.out.print("Ingrese nuevo nombre: ");
                        prod.setNombre(scanf.nextLine());
                        break;
                    case 2:
                        System.out.print("Ingrese nuevo precio: ");
                        prod.setPrecio(Double.parseDouble(scanf.nextLine()));
                        break;
                    case 3:
                        System.out.print("Ingrese nueva categoria: ");
                        String categ = scanf.nextLine();
                        prod.setCategoria(Categorias.valueOf(
                                              categ.toUpperCase()));
                        break;
                    }
                    System.out.print("Desea editar otro parametro (Si/No): ");
                } while(scanf.nextLine().toUpperCase().equals("SI"));
            }
        }
    }

    /**
     * Editar informacion de {@link Producto} registrados
     *
     * @param nombre_producto
     */
    public void editar_productos(String nombre_producto) {
        byte opcion_menu;

        consultar_productos(nombre_producto);
        System.out.println("Ingrese codigo del producto a editar");
        String codigo = scanf.nextLine();
        for (Producto prod : lista_productos) {
            if (prod.getCodigo().equals(codigo)) {
                do {
                    System.out.println("Seleccione parametro a editar");
                    System.out.println("1.Nombre\n2.Precio\n3.Categoria");
                    opcion_menu = Byte.parseByte(scanf.nextLine());
                    switch (opcion_menu) {
                    case 1:
                        System.out.print("Ingrese nuevo nombre: ");
                        prod.setNombre(scanf.nextLine());
                        break;
                    case 2:
                        System.out.print("Ingrese nuevo precio: ");
                        prod.setPrecio(Double.parseDouble(scanf.nextLine()));
                        break;
                    case 3:
                        System.out.print("Ingrese nueva categoria: ");
                        String categ = scanf.nextLine();
                        prod.setCategoria(Categorias.valueOf(
                                              categ.toUpperCase()));
                        break;
                    }
                    System.out.print("Desea editar otro parametro (Si/No): ");
                } while(scanf.nextLine().toUpperCase().equals("SI"));
            }
        }
    }

    /**
     *
     * Editar informacion de {@link Producto} registrados
     * @param categoria
     */
    public void editar_productos(Categorias categoria) {
        byte opcion_menu;

        consultar_productos(categoria);
        System.out.print("Ingrese codigo del producto a editar: ");
        String codigo = scanf.nextLine();
        for (Producto prod : lista_productos) {
            if (prod.getCodigo().equals(codigo)) {
                do {
                    System.out.println("Seleccione parametro a editar");
                    System.out.println("1.Nombre\n2.Precio\n3.Categoria");
                    opcion_menu = Byte.parseByte(scanf.nextLine());
                    switch (opcion_menu) {
                    case 1:
                        System.out.print("Ingrese nuevo nombre: ");
                        prod.setNombre(scanf.nextLine());
                        break;
                    case 2:
                        System.out.print("Ingrese nuevo precio: ");
                        prod.setPrecio(Double.parseDouble(scanf.nextLine()));
                        break;
                    case 3:
                        System.out.print("Ingrese nueva categoria: ");
                        String categ = scanf.nextLine();
                        prod.setCategoria(Categorias.valueOf(
                                              categ.toUpperCase()));
                        break;
                    }
                    System.out.println("Desea editar otro parametro (Si/No): ");
                } while(scanf.nextLine().toUpperCase().equals("SI"));
            }
        }
    }

    /**
     * Editar informacion de {@link Producto} registrados
     *
     * @param nombre_producto
     * @param categoria
     */
    public void editar_productos(String nombre_producto,
                                 Categorias categoria) {
        byte opcion_menu;

        consultar_productos(nombre_producto, categoria);
        System.out.println("Ingrese codigo del producto a editar");
        String codigo = scanf.nextLine();
        for (Producto prod : lista_productos) {
            if (prod.getCodigo().equals(codigo)) {
                do {
                    System.out.println("Seleccione parametro a editar");
                    System.out.println("1.Nombre\n2.Precio\n3.Categoria");
                    opcion_menu = Byte.parseByte(scanf.nextLine());
                    switch (opcion_menu) {
                    case 1:
                        System.out.print("Ingrese nuevo nombre: ");
                        prod.setNombre(scanf.nextLine());
                        break;
                    case 2:
                        System.out.print("Ingrese nuevo precio: ");
                        prod.setPrecio(Double.parseDouble(scanf.nextLine()));
                        break;
                    case 3:
                        System.out.print("Ingrese nueva categoria: ");
                        String categ = scanf.nextLine();
                        prod.setCategoria(Categorias.valueOf(
                                              categ.toUpperCase()));
                        break;
                    }
                    System.out.println("Desea editar otro parametro (Si/No)");
                } while(scanf.nextLine().toUpperCase().equals("SI"));
            }
        }
    }

    /**
     * Consultar la informacion de sus productos registrados filtrando por
     * categoría y nombre
     *
     */
    public void consultar_pedidos() {
        for (Pedido ped : lista_pedidos) { ped.mostrar_detalle_pedido(); }
    }

    /**
     * Gestiona el estado del {@link Producto} a partir de codigo
     *
     * @param cod
     */
    public void gestionar_estado_pedido(String cod) {
        byte opcion_menu = 0;

        for (Pedido ped : lista_pedidos) {
            if (ped.getCodigo_pedido().equals(cod)) {
                System.out.println("Desea marcar pedido como: ");
                System.out.println("1. Procesando (armando el pedido)");
                System.out.println("2. Entregado (entregar el pedido)");
                try {
                    System.out.print("Ingrese opcion: ");
                    opcion_menu = Byte.parseByte(scanf.nextLine());
                }
                catch(NumberFormatException nfe) {
                    System.out.printf(
                        "Error: tipo de dato ingresado invalido --> %s\n",
                        nfe.getMessage());
                }

                switch (opcion_menu) {
                case 1:
                    ped.setEstado_pedido(Estado_Pedido.PROCESANDO);
                    ped.setFecha_procesando(LocalDateTime.now());
                    System.out.println("Procesando con exito");
                    break;
                case 2:
                    ped.setEstado_pedido(Estado_Pedido.ENTREGADO);
                    ped.setFecha_entregado(LocalDateTime.now());
                    System.out.println("Despachado con exito");
                    break;
                }
            }
        }
    }

    /**
     * Ingresa un {@link Pedido} a un ArrayList
     *
     * @param pedido
     */
    public void ingresar_pedido(Pedido pedido) {
        lista_pedidos.add(pedido);
    }

    @Override
    public String toString() {
        return super.toString() + " " + "num_contacto:" + num_contacto;
    }

    /* Getters & Setters */
    public ArrayList < Producto > getLista_productos() {
        return lista_productos;
    }
    public ArrayList < Pedido > getLista_pedidos() {
        return lista_pedidos;
    }
}
