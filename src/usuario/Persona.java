package usuario;

/* local */
import ubicacion.Coordenada;

public abstract class Persona {

    /* Atributos */
    protected String nombre;
    protected String id;
    protected String clave;
    protected String email;
    protected Coordenada ubicacion;

    /* Constructor */
    public Persona(Coordenada ubicacion, String clave, String email, String id,
                   String nombre) {
        this.ubicacion = ubicacion;
        this.clave = clave;
        this.email = email;
        this.id = id;
        this.nombre = nombre;
    }

    /* Getters & Setters */
    public String getClave() { return clave; }
    public void   setClave(String clave) { this.clave = clave; }
    public String getEmail() { return email; }
    public void   setEmail(String email) { this.email = email; }
    public String getId() { return id; }
    public void   setId(String id) { this.id = id; }
    public String getNombre() { return nombre; }
    public void   setNombre(String nombre) { this.nombre = nombre; }
    public Coordenada getUbicacion() { return ubicacion; }

    @Override
    public String toString() {
        return "nombre: " + nombre + ", email: " + email + ", id: " + id +
               ", ubicacion: " + ubicacion.toString();
    }
}
