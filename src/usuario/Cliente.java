package usuario;

/* local */
import java.util.ArrayList;
import pago.MetodoPago;
import procesoventa.Pedido;
import ubicacion.Coordenada;

public class Cliente extends Persona {

    /* Atributos */
    private MetodoPago forma_pago;
    private ArrayList<Pedido> lista_pedido;
    /* Constructor */
    public Cliente(Coordenada ubicacion, String clave, String email, String id,
                   String nombre) {
        super(ubicacion, clave, email, id, nombre);
        lista_pedido =new ArrayList<>();

    }
    /* Metodos */

    public MetodoPago getForma_pago() {
        return forma_pago;
    }
    public void agregar_metodo_pago(MetodoPago metodo) {
        this.forma_pago = metodo;
    }

    @Override
    public String toString() {
        return super.toString() + " " + "forma_pago: " + forma_pago;
    }

    public void setLista_pedido(ArrayList<Pedido> lista_pedido) {
        this.lista_pedido = lista_pedido;
    }
    public ArrayList<Pedido> getLista_pedido() {
        return lista_pedido;
    }
}
