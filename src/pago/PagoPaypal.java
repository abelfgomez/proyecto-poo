package pago;

public class PagoPaypal extends MetodoPago {
    /* Atributos */
    private String nombre_usuario, contrasena;

    /* Constructor */
    public PagoPaypal(String nombre_usuario, String contrasena) {
        this.nombre_usuario = nombre_usuario;
        this.contrasena = contrasena;
    }

    /**
     *
     * Genera un valor al azar entre 100 y 1000 que representara la cantidad de
     * dinero que el cliente tiene en su cuenta y se procesara el pago si los
     * fondos son suficientes
     *
     * @param monto
     */
    @Override
    public boolean proceso_pago(double monto) {
        return (double) (100 + Math.random() * 900) >= monto;
    }
    public static PagoPaypal pedir_datos() {
        System.out.print("Ingrese su nombre de urs-name: ");
        String usr_name = scanf.nextLine();
        System.out.print("Ingrese su clave: ");
        String clave = scanf.nextLine();
        PagoPaypal pagop = new PagoPaypal(usr_name, clave);
        return pagop;
    }

    @Override
    public String toString() {
        return "nombre_usuario: " + nombre_usuario;
    }
}
