package pago;

/* System */
import java.util.Random;

/* Local */
import usuario.Cliente;

public class PagoTarjeta extends MetodoPago implements InterfazGeneral {
    /* Atributos */
    private Tipo_Tarjeta tipo_tarjeta;
    private String numero_tarjeta, nombre_tarjeta;

    /* Constructor */
    public PagoTarjeta(Tipo_Tarjeta tipo_tarjeta, String numero_tarjeta,
                       String nombre_tarjeta, Cliente cliente) {
        super(cliente);
        this.tipo_tarjeta = tipo_tarjeta;
        this.numero_tarjeta = numero_tarjeta;
        this.nombre_tarjeta = nombre_tarjeta;
    }

    /**
     * Genera un codigo aleatorio alfanumerico y se envia este al correo del
     * {@link Cliente}, este debe confirmarlo, se retorna true si es correcto,
     * falso si es erroneo
     *
     * @param monto
     */
    @Override
    public boolean proceso_pago(double monto) {
        Random rd = new Random();
        final String alfa = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        String cadena = "";
        int numero, forma;

        forma = (int) (rd.nextDouble() * alfa.length() - 1 + 0);
        numero = (int) (rd.nextDouble() * 99 + 100);
        cadena = cadena + alfa.charAt(forma) + numero;
        InterfazGeneral.enviar_correo(cte.getEmail(), "Confirmar pago",
                                      cadena);
        System.out.println("Ingrese codigo de confirmacion: ");
        String us_conf = scanf.nextLine();
        if (us_conf.equals(cadena)) {
            System.out.println("Compra validada");
            return true;
        } else {
            System.out.println(
                "Codigo incorrecto, no se pudo validar la compra, el FBI se dirige a su casa");
        }

        return false;
    }

    /**
     * Solicita daatos de la tarjeta de credito
     *
     * @param cliente
     * @return {@link PagoTarjeta}
     */
    public static PagoTarjeta pedir_datos(Cliente cliente) {
        System.out.print("Ingrese Tipo tarjeta: ");
        String tipo_tarjeta = scanf.nextLine();
        System.out.print("Ingrese numero de la tarjeta: ");
        String num_tarjeta = scanf.nextLine();
        System.out.print("Ingrese nombre del titular: ");
        String nombre_titular = scanf.nextLine();
        PagoTarjeta pagoT = new PagoTarjeta(Tipo_Tarjeta.valueOf
                                                (tipo_tarjeta.toUpperCase()),
                                            num_tarjeta, nombre_titular,
                                            cliente);

        return pagoT;
    }

    @Override
    public String toString() {
        return "nombre_tarjeta: " + nombre_tarjeta + ", numero_tarjeta: " +
               numero_tarjeta + ", tipo_tarjeta: " + tipo_tarjeta;
    }

    /* Getters & Setters */
    public Tipo_Tarjeta getTipo_tarjeta() { return tipo_tarjeta; }
    public void setTipo_tarjeta(Tipo_Tarjeta tipo_tarjeta) {
        this.tipo_tarjeta = tipo_tarjeta;
    }
    public String getNumero_tarjeta() { return numero_tarjeta; }
    public String getNombre_tarjeta() { return nombre_tarjeta; }
    public void setNombre_tarjeta(String nombre_tarjeta) {
        this.nombre_tarjeta = nombre_tarjeta;
    }
}
