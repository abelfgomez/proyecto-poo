package pago;

/* System */
import java.util.Properties;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public interface InterfazGeneral {
    /**
     * Envia un correo a un destionatario dando un asunto y mensaje
     *
     * @param destinatario
     * @param asunto
     * @param mensaje
     */
    static void enviar_correo(String destinatario, String asunto,
                              String mensaje) {
        /* Configuracion de cuenta */
        final String CORREO_EMISOR = "farmnearhome@gmail.com";
        final String CLAVE_EMISOR = "proypoo2020";

        Properties propiedad = new Properties();
        propiedad.put("mail.smtp.host", "smtp.gmail.com");
        propiedad.put("mail.smtp.port", "587");
        propiedad.put("mail.smtp.auth", "true");
        propiedad.put("mail.smtp.starttls.enable", "true");
        propiedad.put("mail.smtp.user", CORREO_EMISOR);
        propiedad.put("mail.smtp.clave", CLAVE_EMISOR);

        Session sesion = Session.getDefaultInstance(propiedad);
        MimeMessage mail = new MimeMessage(sesion);

        System.out.println("Precesando envio de mensaje...");
        /* Verifica que el mail se cree de forma correcta */
        try {
            mail.addRecipient(Message.RecipientType.TO,
                              new InternetAddress(destinatario));
            mail.setSubject(asunto);
            mail.setText(mensaje);

            Transport transporte = sesion.getTransport("smtp");
            transporte.connect("smtp.gmail.com", CORREO_EMISOR, CLAVE_EMISOR);
            transporte.sendMessage(mail, mail.getAllRecipients());
            transporte.close();

            System.out.println(
                "Mensaje enviado con exito, revise su bandeja de entrada.");
        } catch(Exception e) {
            e.printStackTrace();
        }
    }
}
