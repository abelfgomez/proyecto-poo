package pago;

/* System */
import java.util.Scanner;

/* Local */
import usuario.Cliente;

public abstract class MetodoPago {
    /* Atributos */
    protected final static Scanner scanf = new Scanner(System.in);
    private double monto;
    Cliente cte;

    /* Constructor */
    public MetodoPago(Cliente cliente) {
        this.cte = cliente;
    }
    public MetodoPago() {}

    /* Metodos */
    public abstract boolean proceso_pago(double monto);

    /* Getters & Setters */
    public double getMonto() { return monto; }
    public void   setMonto(double monto) { this.monto = monto; }
}
